/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react';
import Router from 'react-routing/src/Router';
import http from './core/HttpClient';
import App from './components/App';
import TestActions from './actions/TestsActions';
import TestStore from './stores/TestsStore';
import TestsPage from './components/TestsPage';
import TestPage from './components/TestPage';
import NotFoundPage from './components/NotFoundPage';
import ErrorPage from './components/ErrorPage';
import { Provider, connect } from 'react-redux'

const router = new Router(on => {
  on('*', async (state, next) => {
    const component = await next();
    return component && <App context={state.context}><Provider store={TestStore}>{component}</Provider></App>;
  });

  on('/', async () => {
    let state = TestStore.getState();
    /// Load tests only when there are none in state
    if (state.tests.length == 0) {
      TestActions.loadTests().then(res => TestActions.dispatchTests(res));
    }
    return <TestsPage/>;
  });

  on('/test/:test', async(state) => {
    TestActions.loadTest(state.params.test).then(res => TestActions.dispatchTestLoaded(res));
    return <TestPage/>;
  });

  on('error', (state, error) => state.statusCode === 404 ?
      <App error={error}><NotFoundPage /></App>:
      <App error={error}><ErrorPage /></App>
  );
});

export default router;
