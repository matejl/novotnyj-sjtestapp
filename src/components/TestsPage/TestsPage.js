/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import Actions from '../../actions/TestsActions';
import Link from '../Link';
import TestStore from '../../stores/TestsStore';
import { connect } from 'react-redux';

class TestsPage extends Component {

  static contextTypes = {

    tests: PropTypes.array,
    submitted: PropTypes.array
  };

  render() {
    const {tests, submitted} = this.props;
    return (
      <div className="ContactPage">
          <h1>Tests</h1>
          {submitted.map(test => {
            return (<div key={test.id} className="alert alert-success">
              {test.name} submitted!
            </div>)
          })}
          {tests.map(test => {
            return (
              <Link key={test.id} to={"/test/" + test.id}>
                <div className="panel panel-default">
                  <div className="panel-body">
                    {test.name}
                    <span className="pull-right glyphicon glyphicon-chevron-right"></span>
                  </div>
                </div>
              </Link>);
          })}
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    tests: state.tests,
    submitted: state.submitted
  };
}

export default connect(mapStateToProps)(TestsPage);

