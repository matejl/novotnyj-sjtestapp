import React, {Component, PropTypes} from 'react';

export default class Answer extends Component {

  static propTypes = {
    answer: PropTypes.shape({
      id: PropTypes.number.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired,
    name: PropTypes.string.isRequired,
    onChecked: PropTypes.func
  };

  handleCheck() {
    this.props.onChecked(this.props.answer.id)
  }

  render() {
    return (
      <div className="radio">
        <label>
          <input type="radio"
                 name={this.props.name}
                 onChange={this.handleCheck.bind(this)}
                 value={this.props.answer.id}/>
          {this.props.answer.text}
        </label>
      </div>
    );
  }

}
