import React, {Component, PropTypes} from 'react';
import Answer from '../Answer';

export default class Question extends Component {

  static propTypes = {
    question: PropTypes.shape({
      id: PropTypes.number.isRequired,
      text: PropTypes.string.isRequired,
      answers: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          text: PropTypes.string.isRequired
        })
      ).isRequired
    }),
    onAnswer: PropTypes.func
  };

  handleAnswer(answerId) {
    this.props.onAnswer(this.props.question.id, answerId);
  }

  render() {
    return (
        <div className="panel panel-default">
            <div className="panel-heading">{this.props.question.id}: {this.props.question.text}</div>
            <div className="panel-body">
              {this.props.question.answers.map(answer => {
                return (
                  <Answer key={answer.id}
                          name={"Answer" + this.props.question.id}
                          onChecked={this.handleAnswer.bind(this)}
                          answer={answer} />
                );
              })}
            </div>
        </div>
    );
  }

}
