import React, {Component, PropTypes} from 'react';
import TestStore from '../../stores/TestsStore';
import TestActions from '../../actions/TestsActions';
import Question from '../Question';
import Location from '../../core/Location';
import { connect } from 'react-redux';

class TestPage extends Component {

  static propTypes = {
    test: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      intro: PropTypes.string,
      questions: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number
      }))
    })
  };

  handleAnswer(questionId, answerId) {
    TestActions.dispatchAnswer(this.props.test.id, questionId, answerId);
  }

  handleSubmit() {
    let state = TestStore.getState();
    /// If there are any answers - submit
    if ("answers" in state.edited) {
      TestActions.postTest(state.edited).then(() => {
        TestActions.dispatchTestSubmitted(this.props.test);
        Location.pushState('/');
      });
    }
  }

  render() {
    const test = this.props.test;
    return (
      <div>
        <h1>{test.name}</h1>
        <p>{test.intro}</p>
        {test.questions.map(question => {
          return (<Question
            key={question.id}
            onAnswer={this.handleAnswer.bind(this)}
            question={question} />
          );
        })}
        <div className="pull-right">
          <button className="btn btn-success" onClick={this.handleSubmit.bind(this)} hidden={test.id==0}>
            Submit
          </button>
        </div>
      </div>
    );
  }

}

export default connect(state => {return {test: state.test}; })(TestPage);
