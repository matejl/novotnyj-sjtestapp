/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
  TestsLoaded: null,
  TestLoaded: null,
  Answer: null,
  TestSubmitted: null
});
