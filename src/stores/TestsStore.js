import { createStore } from 'redux';
import Actions from '../constants/ActionTypes';
import { canUseDOM } from 'fbjs/lib/ExecutionEnvironment';

const initialState = canUseDOM ? window.__INITIAL_STATE__ : {
  tests: [],
  test:{
    id: 0,
    name: "",
    info: "",
    questions:[]
  },
  edited: {},
  submitted: []
};

function testsReducer(state = initialState, action) {
  switch (action.type) {
    case Actions.TestsLoaded:
          return {...state, tests: action.data};
    case Actions.TestLoaded:
          return {...state, test: action.data, edited: {}, submitted: []};
    case Actions.Answer:
          let answers = [];
          /// Don't answer one question multiple times
          if ("answers" in state.edited) {
            answers = state.edited.answers.filter(answer => answer.question_id != action.data.question);
          }

          /// Add current answer to collection
          answers.push({
            question_id: action.data.question,
            answer: action.data.answer
          });

          return {...state, edited: {
            test_id: action.data.test,
            answers: answers
          }};
    case Actions.TestSubmitted:
          return {...state, submitted: [action.data]};
    default:
          return state;
  }
}

var store = createStore(testsReducer);

export default store;
