import HttpClient from '../core/HttpClient';
import TestStore from '../stores/TestsStore';
import Actions from '../constants/ActionTypes';

export default class TestsActions {

  static dispatchTests(tests) {
    TestStore.dispatch({type: Actions.TestsLoaded, data: tests});
  }

  static dispatchAnswer(test, question, answer) {
    TestStore.dispatch({
      type: Actions.Answer,
      data: {
        test: test,
        question: question,
        answer: answer
      }
    });
  }

  static dispatchTestSubmitted(test) {
    TestStore.dispatch({type: Actions.TestSubmitted, data: test});
  }

  static dispatchTestLoaded(test) {
    TestStore.dispatch({type: Actions.TestLoaded, data: test});
  }

  static async loadTests() {
    return HttpClient.get('http://private-b9c64-sjtestappapi.apiary-mock.com/api/tests');
  }

  static async loadTest(testId) {
    return HttpClient.get('http://private-b9c64-sjtestappapi.apiary-mock.com/api/tests/' + testId);
  }

  static postTest(test) {
    return HttpClient.post('http://private-b9c64-sjtestappapi.apiary-mock.com/api/tests', test);
  }

}
