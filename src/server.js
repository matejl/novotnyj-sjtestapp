/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import 'babel-core/polyfill';
import path from 'path';
import express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import Router from './routes';
import Html from './components/Html';
import http from './core/HttpClient';
import TestStore from './stores/TestsStore';
import TestActions from './actions/TestsActions';

const server = global.server = express();

server.set('port', (process.env.PORT || 5000));
server.use(express.static(path.join(__dirname, 'public')));

server.get('/', async(req, res, next) => {
  /// Load tests before render
  let tests = await TestActions.loadTests();
  TestActions.dispatchTests(tests);
  next();
});

// Register server-side rendering middleware
server.get('*', async (req, res, next) => {
  try {
    let statusCode = 200;

    const data = { title: '', description: '', css: '', body: '', initialState: {} };

    const context = {
      onPageNotFound: () => statusCode = 404
    };

    await Router.dispatch({ path: req.path, context }, (state, component) => {
      data.body = ReactDOM.renderToString(component);
    });

    data.initialState = TestStore.getState();

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
    res.status(statusCode).send('<!doctype html>\n' + html);
  } catch (err) {
    next(err);
  }
});

//
// Launch the server
// -----------------------------------------------------------------------------

server.listen(server.get('port'), () => {
  /* eslint-disable no-console */
  console.log('The server is running at http://localhost:' + server.get('port'));
  if (process.send) {
    process.send('online');
  }
});
